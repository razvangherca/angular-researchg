# CourseApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Purpose

The purpose of this project was to do some research on both Angular and Typescript, get familiar with them and to see if it would fit my career development needs.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

